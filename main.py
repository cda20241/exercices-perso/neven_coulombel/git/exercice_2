""" Git Exercice 2 """

from somme import somme, somme3nombres
from soustraction import soustraction

if __name__ == '__main__':
    print('Hello, World!')
    print('La somme de 2 et 6,2 vaut : ', somme(2, 6.2))
    print('La somme de 2 et 6,2 et 10 vaut : ', somme3nombres(2, 6.2, 10))
    print('La soustraction de 5 et 2 vaut : ', soustraction(5, 2))
