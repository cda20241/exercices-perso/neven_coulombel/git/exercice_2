""" Fonction Soustraction """


def soustraction(a: float, b: float) -> float:
    """
    Soustraction des 2 paramètres
    :param a: 'float'
    :param b: 'float'
    :return: 'float'
    """
    return a - b
