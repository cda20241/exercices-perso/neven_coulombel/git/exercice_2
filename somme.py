""" Fonctions Sommes """


def somme(a: float, b: float) -> float:
    """
    Addition des 2 paramètres
    :param a: 'float'
    :param b: 'float'
    :return: 'float'
    """
    return a + b


def somme3nombres(a: float, b: float, c: float) -> float:
    """
    Addition des 3 paramètres
    :param a: 'float'
    :param b: 'float'
    :param c: 'float'
    :return: 'float'
    """
    return a + b + c
